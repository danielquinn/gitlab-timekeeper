# Timekeeper

GitLab has fabulous time-tracking, but reporting for it is currently lacking,
especially if you want to collect your time spent across issues & milestones.

This is a simple Python script to go back through a project's history and
generate some collected time output:

```
$ timekeeper --project-id 123 --token=abcd1234 --look-back 2019-01-01T00:00:00.000Z

Id   Issue                                       Time  Closed At
===============================================================================
2307 Fix the knob on that thing                  1.50  2019-02-04T20:11:09.952Z
2304 Update the other thing                      2.25  -
===============================================================================
Total:                                           3.75
```
